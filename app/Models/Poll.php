<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Poll extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function choices()
    {
        return $this->hasMany(Choice::class);
    }
}
