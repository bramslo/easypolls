<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Poll;

class PollsController extends Controller
{
    public function index()
    {
        $polls = Poll::all();

        return view('polls.index', compact('polls'));
    }

    public function create()
    {
        return view('polls.create');
    }

    public function show(Poll $poll)
    {
        return view('polls.show', compact('poll'));
    }

    public function store(Request $request)
    {
        $poll = Poll::create([
            'question_text' => $request->get('question_text'),
        ]);
        if ($poll) {
            for ($i=1; $i < 4 ; $i++) { 
                $poll->choices()->create([
                    'poll_id'   => $poll->id ,
                    'name'      => $request->get('question_choice_'.$i)
                ]);
            }
        }

        session()->flash('notification.success', 'Sondage créé avec succès!');

        return redirect(route('polls.show', $poll));
    }
}
